#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

// a struct that represents the top level of the pokedex
typedef struct Pokedex {

  
  struct PlayerNode *PlayerList; // a pointer to the head of the PlayerList
  struct PokemonNode *PokemonList;// a pointer to the head of the PokemonList

}Pokedex;

// a struct that represents the top level of the pokemon node
typedef struct PokemonNode {
    char name[20];
    char type[20];
    char ability[20];
    struct PokemonNode *next; // a pointer to next pokemon in the list

} PokemonNode;


// a struct that represents the top level of the player node
typedef struct PlayerNode {
    char name[20];
    int Pokemon_count;
    PokemonNode* PokemonsOwned[20];  // an array that contains pointers to all the pokemons that a player owns
    struct PlayerNode *next;  // a pointer to next Player in the list
    
} PlayerNode;



//function declarations
PlayerNode* NewPlayerNode(char name[20]);
void AddPlayerToPlayerList(Pokedex *pokedex, char name[20]);
PlayerNode* FindPlayer(Pokedex pokedex, char name[20]);

PokemonNode* NewPokemonNode(char name[20], char type[20], char ability[20]);
void AddPokemonToPokemonList(Pokedex *pokedex, char name[20], char type[20], char ability[20]);
PokemonNode* FindPokemon(Pokedex pokedex, char name[20]);

void DisplayPokemonDetails(Pokedex, char name[20]);
void DisplayPlayerDetails(Pokedex, char name[20]);
void ListPokemons(Pokedex pokedex);
void ListPlayers(Pokedex pokedex);



// the main function
int main(void) {
    
    Pokedex pokedex;

    pokedex.PlayerList = NULL;     // head of PlayerList
    pokedex.PokemonList = NULL;    // head of PokemonList

    
//Adding players to player list
    AddPlayerToPlayerList(&pokedex, "Misty");
    AddPlayerToPlayerList(&pokedex, "Brock");
    AddPlayerToPlayerList(&pokedex, "Tracey");
    AddPlayerToPlayerList(&pokedex, "Dawn");
    AddPlayerToPlayerList(&pokedex, "Iris");
    AddPlayerToPlayerList(&pokedex, "Cilan");
    AddPlayerToPlayerList(&pokedex, "Serena");
    AddPlayerToPlayerList(&pokedex, "Clemont");
    AddPlayerToPlayerList(&pokedex, "Lana");
    AddPlayerToPlayerList(&pokedex, "Cloe");


 //Adding pokemons to pokemon list
    AddPokemonToPokemonList(&pokedex, "Bulbasaur", "Grass", "Poison");
    AddPokemonToPokemonList(&pokedex, "Butterfree", "Bug", "Flying");
    AddPokemonToPokemonList(&pokedex, "Pidgee", "Normal", "Flying");
    AddPokemonToPokemonList(&pokedex, "Pikachu", "Electric", "Shock");
    AddPokemonToPokemonList(&pokedex, "NidoQueen", "Poison", "Vapour");
    AddPokemonToPokemonList(&pokedex, "Jigglypuff", "Normal", "Fairy");
    AddPokemonToPokemonList(&pokedex, "Golbat", "Poison", "Flying");
    AddPokemonToPokemonList(&pokedex, "Poliwrath", "Water", "Fighting");
    AddPokemonToPokemonList(&pokedex, "Tentacool", "Water", "Poison");
    AddPokemonToPokemonList(&pokedex, "Geodude", "Rock", "Ground");


    /*  User menu options
    PokemonNode *head= NULL;
    while(choice!= '6'){

    
    printf("\n **********Pokedex Program Menu**********");
    printf("\n 1.See all the Pokemons from Pokedex Program");
    printf("\n 2.See all the Players and Pokemons owned from Pokedex Program");
    printf("\n 3.Add a new Pokemon");
    printf("\n 4.Add a new Player");
    printf("\n 5.See a Pokemon details");
    printf("\n 6.See a Player details");
    printf("\n Please select your choice");

    
    switch choice{
        case '1':
        break;
        case '2':
        break;
        case '3':
        break;
        case '4':
        break;
        case '5':
        break;
        case '6':
        break;

        default:
        printf("\n Invalid Option....Please choose again carefully");
        
    } 
    return 0;
    */
};
 
// PlayerList functions

// A function that creates a new node and returns a pointer to that node and sets pokemon count to 0

PlayerNode* NewPlayerNode( char name[20]){
    PlayerNode* NewPlayerNode = NULL;                    // creating a pointer to PlayerNode and setting it to NULL for safety
    NewPlayerNode = malloc(sizeof(PlayerNode));         // setting aside space for the node structure
    NewPlayerNode -> Pokemon_count = 0;                // setting pokemon count to 0
    strcpy(NewPlayerNode -> name, name);              // sintax used to pass the whole name 
    NewPlayerNode -> next = NULL;
    return NewPlayerNode;                           // returns a pointer to the NewPlayerNode
}

// A function that checks if the name already exists in PlayerList.
// If it doesn't creates a new node and adds it to the list, if it does then do nothing

void AddPlayerToPlayerList( Pokedex *pokedex, char name[20]){
    // creates pointer to head of list
    PlayerNode *Itterator = pokedex->PlayerList; 
    //traverse the list
    while (Itterator != NULL){
        //check name
        if(strcmp(name,Itterator->name) == 0){
        //if name exists do nothing 
        }
        else{
          //check next 
          Itterator = Itterator -> next;

        }
    }
}

PlayerNode* FindPlayer(Pokedex pokedex,char name[20]){
    PlayerNode* Iterator = pokedex.PlayerList;

    // while name != keep looping
    while (Iterator != NULL){
      if(strcmp(name, Iterator -> name) == 0){
        return Iterator;
      } 
    Iterator = Iterator->next;
    }
      //else
      return NULL;
    }
    



// A function that creates a new node and returns a pointer to that node
PokemonNode* NewPokemonNode(char name[20], char type[20], char ability[20]){
    PokemonNode* NewPokemonNode = NULL;
    NewPokemonNode = malloc(sizeof(PokemonNode));
    strcpy(NewPokemonNode -> name, name);
    strcpy(NewPokemonNode -> type, type);
    strcpy(NewPokemonNode -> ability, ability);
    NewPokemonNode -> next = NULL;
    return NewPokemonNode;
}


// A function that adds a pokemon to the list

void AddPokemonToPokemonList(Pokedex *pokedex, char name[20], char type[20], char ability[20]){
    //creating pointer to head of pokemon list
    PokemonNode *Iterator = pokedex->PokemonList;
    //traversing pokemon list
    while (Iterator != NULL){
        //checking name
        if(strcmp(name, Iterator->name)==0){
            //if name exists do nothing 
        }
        else{
          //Check next
          Iterator = Iterator -> next;
        }
    }
}


//A function that searches the pokemon list for name. If is finds name then returns a pointer to that name's node otherwise returns NULL

PokemonNode* FindPokemon(Pokedex pokedex, char name[20]){
    PokemonNode *Iterator = pokedex.PokemonList;
    while (Iterator != NULL){
      if(strcmp(name, Iterator -> name) == 0){
        return Iterator;

      }
      Iterator = Iterator->next;
    }
    //else
    return NULL;
}


// Functions for Pokedex

// A function that outputs the details of Pokemon name details to the screen

void DisplayPokemonDetails(Pokedex pokedex, char name[20]){
    PokemonNode* Iterator = pokedex.PokemonList;

    while(Iterator != NULL){
      if(strcmp(Iterator -> name, name) == 0){
        printf("Selected pokemon details are: %s\n", Iterator -> name);
        printf("Type:%s\n", Iterator -> type);
        printf("Ability%s\n", Iterator -> ability);

        Iterator = Iterator -> next;
    }
}
}

// A function that outputs the details of player name to the screen including a list of pokemons owned by that player

void DisplayPlayerDetails(Pokedex pokedex, char name[20]){
    PlayerNode* Iterator = pokedex.PlayerList;

    while(Iterator != NULL){

      if(strcmp(Iterator -> name, name) == 0){

        printf("Selected player details are: %s\n", Iterator -> name);
        printf("Number of pokemons owned:%i\n", Iterator -> Pokemon_count);

      } 

      Iterator = Iterator -> next;

    }
   
}

// A function that outputs a list of names of all pokemon in the pokedex

void ListPokemons(Pokedex pokedex){
    PokemonNode *Iterator = pokedex.PokemonList;
    printf("All Pokemons in the Pokedex are: %s/n", Iterator->name);
    while(Iterator !=NULL){
        printf("%s\n", Iterator->name);
        Iterator = Iterator -> next;
    }
}


// A function that outputs a list of all players in the pokedex

void ListPlayers(Pokedex pokedex){
    PlayerNode *Iterator = pokedex.PlayerList;
    printf("All players in the Pokedex are:%s/n", Iterator -> name);
    while(Iterator != NULL){
        printf("%s\n", Iterator -> name);
        Iterator = Iterator -> next;
       
    }
}


